package gpapp.martingdropin.com.gp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gpapp.martingdropin.com.gp.helper.Constants;
import gpapp.martingdropin.com.gp.helper.Helpers;

public class AccountUpdateActivity extends AppCompatActivity {

    @Bind(R.id.nameTf)
    EditText nameTf;
    @Bind(R.id.addressText)
    EditText mAddressTf;
    @Bind(R.id.postCodeTf)
    EditText mPostCodeTf;
    @Bind(R.id.nhsNumberTf)
    EditText mNHSNumberTf;
    @Bind(R.id.emailTf)
    EditText mEmailTf;
    @Bind(R.id.contactTf)
    EditText mContactNumberTf;

        ParseObject UserObject;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_account_update);
            ButterKnife.bind(this);
            fetchUserDetails();
        }
        // Register button on click functionality
        @OnClick(R.id.updateUser)
        void updateAccount()
        {
            if(validateFields())
            {
                final ParseUser user = ParseUser.getCurrentUser();
                if(user!=null) {
                    updateUserDetails(user);
                }
            }
        }

        void fetchUserDetails()
        {
            final ParseUser user = ParseUser.getCurrentUser();
            if(user!=null) {
                final Helpers helpers = new Helpers(this);
                helpers.showLoadingDialog();
                ParseQuery<ParseObject> query = ParseQuery.getQuery("NormalUser");
                query.whereEqualTo(Constants.USER_POINTER_COLUMN_CONSTANT, user);
                query.getFirstInBackground(new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject object, ParseException e) {
                        helpers.dismissLoadingDialog();
                        if(e==null) {
                            UserObject = object;
                            showUserDetails(user);
                        }
                        else
                            Helpers.showToast(getApplicationContext(),e.getMessage());
                    }
                });
            }
        }

        void showUserDetails(ParseUser user)
        {
            nameTf.setText(UserObject.getString(Constants.CONSTANT_NORMAL_NAME_COLUMN));
            mAddressTf.setText(UserObject.getString(Constants.CONSTANT_NORMAL_ADDRESS_COLUMN));
            mContactNumberTf.setText(UserObject.getString(Constants.CONSTANT_NORMAL_CONTACT_COLUMN));
            mPostCodeTf.setText(UserObject.getString(Constants.CONSTANT_NORMAL_POSTCODE_COLUMN));
            mNHSNumberTf.setText(UserObject.getString(Constants.CONSTANT_NORMAL_NHS_COLUMN));
            mEmailTf.setText(user.getEmail());
        }

        void updateUserDetails(ParseUser user)
        {
            if(UserObject!=null)
            {
                final Helpers helpers = new Helpers(this);
                helpers.showLoadingDialog();

                UserObject.put(Constants.CONSTANT_NORMAL_NAME_COLUMN, nameTf.getText().toString());
                UserObject.put(Constants.CONSTANT_NORMAL_ADDRESS_COLUMN, mAddressTf.getText().toString());
                UserObject.put(Constants.CONSTANT_NORMAL_POSTCODE_COLUMN, mPostCodeTf.getText().toString());
                UserObject.put(Constants.CONSTANT_NORMAL_NHS_COLUMN, mNHSNumberTf.getText().toString());
                UserObject.put(Constants.CONSTANT_NORMAL_CONTACT_COLUMN, mContactNumberTf.getText().toString());

                this.UserObject.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            helpers.dismissLoadingDialog();
                            Helpers.showToast(getApplicationContext(), getString(R.string.update_success_message));
                        } else {
                            helpers.dismissLoadingDialog();
                            Helpers.showToast(getApplicationContext(), e.getMessage());
                        }
                    }
                });
            }
        }
        boolean validateFields()
        {
            if(nameTf.getText().toString().isEmpty()||
                    mContactNumberTf.getText().toString().isEmpty() ||
                    mNHSNumberTf.getText().toString().isEmpty()||
                    mAddressTf.getText().toString().isEmpty()||
                    mPostCodeTf.getText().toString().isEmpty())
            {
                Helpers.showToast(this, "All fields are compulsory");
                return false;
            }

            return true;
        }
}

