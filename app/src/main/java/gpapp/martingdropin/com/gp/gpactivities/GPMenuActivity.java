package gpapp.martingdropin.com.gp.gpactivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.parse.ParseUser;

import gpapp.martingdropin.com.gp.NewsUpdateActivity;
import gpapp.martingdropin.com.gp.R;
import gpapp.martingdropin.com.gp.adapters.MenuAdapter;
import gpapp.martingdropin.com.gp.helper.Constants;
import it.gmariotti.recyclerview.adapter.AlphaAnimatorAdapter;

public class GPMenuActivity extends AppCompatActivity implements MenuAdapter.RecyclerViewListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gp_menu);
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        MenuAdapter menuAdapter = new MenuAdapter(Constants.GP_MENU_ARRAY,this);
        AlphaAnimatorAdapter animatorAdapter = new AlphaAnimatorAdapter(menuAdapter,recyclerView);
        recyclerView.setAdapter(animatorAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
    }

    @Override
    public void recyclerListViewClicked(View v, int position) {

        switch (position)
        {
            case 0:
                Intent intent = getPackageManager().getLaunchIntentForPackage("com.android.email");
                startActivity(intent);
            case 1:
                Intent appointmentIntent =  new Intent(this,AppointmentsGPActivity.class);
                startActivity(appointmentIntent);
                break;
            case 2:
                Intent prescriptionIntent =  new Intent(this,PrescriptionGPActivity.class);
                startActivity(prescriptionIntent);
                break;
            case 3:
                Intent feedbackIntent =  new Intent(this,FeedbackGPActivity.class);
                startActivity(feedbackIntent);
                break;
            case 4:
                Intent newsUpdateIntent =  new Intent(this,NewsUpdateActivity.class);
                startActivity(newsUpdateIntent);
                break;
            case 5:
                Intent updateAccountIntent =  new Intent(this,UpdateGpActivity.class);
                startActivity(updateAccountIntent);
                break;
            case 6:
                finish();
                ParseUser.logOut();

        }

    }
    @Override
    public void onBackPressed() {

    }
}
