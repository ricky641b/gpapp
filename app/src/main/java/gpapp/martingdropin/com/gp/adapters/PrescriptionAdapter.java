package gpapp.martingdropin.com.gp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.ParseObject;

import java.util.List;

import gpapp.martingdropin.com.gp.PrescriptionDetailActivity;
import gpapp.martingdropin.com.gp.R;

/**
 * Created by RAJESHGUPTA on 22/02/16.
 */
public class PrescriptionAdapter extends RecyclerView.Adapter<PrescriptionAdapter.AppointmentsViewHolder>{

    private List<ParseObject> appointmentsList;
    public PrescriptionAdapter(List<ParseObject> newsList,Context context)
    {
        appointmentsList = newsList;
        mContext = context;
    }
    private Context mContext;
    public class AppointmentsViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener
    {
        private TextView mPrescription;

        public AppointmentsViewHolder(View itemView) {
            super(itemView);
            mPrescription = (TextView) itemView.findViewById(R.id.prescriptionText);
            itemView.setOnClickListener(this);
        }
        public void bindNews(int position)
        {
            ParseObject prescriptionObject = appointmentsList.get(position);
            mPrescription.setText(prescriptionObject.getString("prescriptions"));
        }
        @Override
        public void onClick(View v) {
            Intent prescriptionDetail = new Intent(mContext, PrescriptionDetailActivity.class);
            String parseObjectId = appointmentsList.get(getAdapterPosition()).getObjectId();
            prescriptionDetail.putExtra("objectId",parseObjectId);
            mContext.startActivity(prescriptionDetail);
        }
    }
    public AppointmentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.prescription_list_item,parent,false);
        AppointmentsViewHolder viewHolder = new AppointmentsViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AppointmentsViewHolder holder, int position)
    {
        holder.bindNews(position);
    }

    @Override
    public int getItemCount() {
        return appointmentsList.size();
    }

}

