package gpapp.martingdropin.com.gp.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.ParseObject;

import java.util.List;

import gpapp.martingdropin.com.gp.R;
import gpapp.martingdropin.com.gp.helper.Constants;

/**
 * Created by RAJESHGUPTA on 22/02/16.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder>{

    private List<ParseObject> news;
    private Context mContext;
    public NewsAdapter(List<ParseObject> newsList, Context context)
    {
        news = newsList;
        mContext = context;

    }

    public class NewsViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener
    {
        private TextView mNewsTitle;
        private TextView mNewsDescription;
        public NewsViewHolder(View itemView) {
            super(itemView);
            mNewsTitle = (TextView) itemView.findViewById(R.id.newsTitle);
            mNewsDescription = (TextView) itemView.findViewById(R.id.newsDescription);
            itemView.setOnClickListener(this);
        }
        public void bindNews(int position)
        {
            ParseObject newsObject = news.get(position);
            mNewsTitle.setText(newsObject.getString(Constants.NEWS_TITLE_COLUMN_CONSTANT));
            mNewsDescription.setText(newsObject.getString(Constants.NEWS_DESCRIPTION_COLUMN_CONSTANT));
        }
        @Override
        public void onClick(View v) {
            ParseObject newsObject = news.get(getAdapterPosition());
            String link = newsObject.getString(Constants.NEWS_LINK_COLUMN_CONSTANT);
            if(link!=null || !link.isEmpty()) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                mContext.startActivity(browserIntent);
            }
        }

    }
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_list_item,null,false);
        NewsViewHolder viewHolder = new NewsViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position)
    {
        holder.bindNews(position);
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

}

