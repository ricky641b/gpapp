package gpapp.martingdropin.com.gp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.ParseObject;

import java.util.List;

import gpapp.martingdropin.com.gp.R;
import gpapp.martingdropin.com.gp.helper.Constants;

/**
 * Created by RAJESHGUPTA on 22/02/16.
 */
public class AppointmentsAdapter extends RecyclerView.Adapter<AppointmentsAdapter.AppointmentsViewHolder>{

    private List<ParseObject> appointmentsList;
    public AppointmentsAdapter(List<ParseObject> newsList)
    {
        appointmentsList = newsList;
    }

    public class AppointmentsViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener
    {
        private TextView mPatientName;
        private TextView mDateOfAppointment;
        private TextView mReasonForAppointment;

        public AppointmentsViewHolder(View itemView) {
            super(itemView);
            mPatientName = (TextView) itemView.findViewById(R.id.patientName);
            mDateOfAppointment = (TextView) itemView.findViewById(R.id.dateOfAppointment);
            mReasonForAppointment = (TextView) itemView.findViewById(R.id.reasonForAppointment);
            itemView.setOnClickListener(this);
        }
        public void bindNews(int position)
        {
            ParseObject appointmentObject = appointmentsList.get(position);
            mPatientName.setText("Name: " + appointmentObject.getString(Constants.CONSTANT_APPOINTMENT_PATIENT_NAME_COLUMN));
            mReasonForAppointment.setText("Reason: " + appointmentObject.getString(Constants.CONSTANT_APPOINTMENT_REASON_COLUMN));
            mDateOfAppointment.setText("Date: " + appointmentObject.getString(Constants.CONSTANT_APPOINTMENT_DOB_COLUMN));
        }
        @Override
        public void onClick(View v) {

        }

    }
    public AppointmentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.appointment_list_item,parent,false);
        AppointmentsViewHolder viewHolder = new AppointmentsViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AppointmentsViewHolder holder, int position)
    {
        holder.bindNews(position);
    }

    @Override
    public int getItemCount() {
        return appointmentsList.size();
    }

}

