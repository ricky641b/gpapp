package gpapp.martingdropin.com.gp;

import com.parse.Parse;

/**
 * Created by RAJESHGUPTA on 21/02/16.
 */
public class MyApp extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Parse.enableLocalDatastore(this);
        Parse.initialize(this);
    }
}
