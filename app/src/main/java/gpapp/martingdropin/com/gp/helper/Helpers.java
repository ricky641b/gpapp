package gpapp.martingdropin.com.gp.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import gpapp.martingdropin.com.gp.R;

/**
 * Created by RAJESHGUPTA on 22/02/16.
 */
public class Helpers {
    private ProgressDialog progress;
    Context context;
    public static void showToast(Context ctx,String message)
    {
        Toast.makeText(ctx,message,Toast.LENGTH_SHORT).show();
    }

    public Helpers(Context ctx)
    {
        context = ctx;
    }
    public void showLoadingDialog() {

        if (progress == null) {
            progress = new ProgressDialog(context);
            progress.setIndeterminate(true);
            progress.setTitle(context.getString(R.string.loading_title));
            progress.setMessage(context.getString(R.string.loading_message));
            progress.setCancelable(false);
        }
        progress.show();
    }

    public void dismissLoadingDialog() {

        if (progress != null && progress.isShowing()) {
            progress.dismiss();
        }
    }
}
