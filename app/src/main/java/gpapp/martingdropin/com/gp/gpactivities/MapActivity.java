package gpapp.martingdropin.com.gp.gpactivities;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gpapp.martingdropin.com.gp.GpDetailActivity;
import gpapp.martingdropin.com.gp.R;
import gpapp.martingdropin.com.gp.helper.Helpers;
import gpapp.martingdropin.com.gp.helper.LocationProvider;

public class MapActivity extends FragmentActivity implements LocationProvider.LocationCallback {

    public static final String TAG = MapActivity.class.getSimpleName();
    private static final int REQUEST_CODE = 12;
    private GoogleMap mMap; // Might be null if Google Play seravices APK is not available.
    PlaceAutocompleteFragment autocompleteFragment;
    private LocationProvider mLocationProvider;
    private String mReasonString;
    private String mDateString;
    private HashMap<String, ParseObject> markers= new HashMap<String,ParseObject>();
    private ParseObject mGPUserObject;
    @Bind(R.id.bookAppointment)
    Button bookAppointment;
    boolean isSearching;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);
        mReasonString = getIntent().hasExtra("REASON")?getIntent().getExtras().getString("REASON"):"";
        mDateString =  getIntent().hasExtra("DATE")?getIntent().getExtras().getString("DATE"):"";
        bookAppointment.setVisibility(View.INVISIBLE);
        setUpMapIfNeeded();
        mLocationProvider = new LocationProvider(this, this);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                marker.showInfoWindow();
                ParseObject parseObject = markers.get(marker.getId());
                if (parseObject != null) {
                    mGPUserObject = parseObject;
                    if (!mReasonString.isEmpty()) {
                        bookAppointment.setVisibility(View.VISIBLE);
                    }
                }
                return true;
            }
        });
        autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                Log.d("SOME", "SOME");
                isSearching = true;
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 12));
                // txtPlaceDetails.setText(placeDetailsStr);
            }

            @Override
            public void onError(Status status) {
                Log.d("STATUS", status.getStatusMessage());
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        mLocationProvider.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLocationProvider.disconnect();
    }

    @OnClick(R.id.bookAppointment)
    void bookAppointment()
    {
        if(mGPUserObject!=null) {
            Intent gpdetailIntent = new Intent(this, GpDetailActivity.class);
            gpdetailIntent.putExtra("objectId",mGPUserObject.getObjectId());
            gpdetailIntent.putExtra("REASON", mReasonString);
            gpdetailIntent.putExtra("DATE", mDateString);
            startActivityForResult(gpdetailIntent, REQUEST_CODE);
        }
        else
        {
            Helpers.showToast(this,"Please select GP before booking appointment");
        }
    }
    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }

    }

    void findAllGP(ParseGeoPoint parseGeoPoint)
    {
        final Helpers helpers = new Helpers(this);
        helpers.showLoadingDialog();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GPUser");
        query.whereNear("location", parseGeoPoint);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                helpers.dismissLoadingDialog();
                if(e==null) {
                    markers.clear();
                    for (ParseObject object : objects) {
                        ParseGeoPoint geoPoint = object.getParseGeoPoint("location");
                        LatLng latLng = new LatLng(geoPoint.getLatitude(), geoPoint.getLongitude());
                        MarkerOptions options = new MarkerOptions()
                                .position(latLng)
                                .title("Dr. " + object.getString("fullName"));
                        markers.put(mMap.addMarker(options).getId(),object);
                    }
                }
                else
                {
                    Helpers.showToast(getApplicationContext(),"Sorry! No GP found around your area");
                }
            }
        });
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        //mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }

    public void handleNewLocation(Location location) {


        if(!isSearching) {
            double currentLatitude = location.getLatitude();
            double currentLongitude = location.getLongitude();
            LatLng latLng = new LatLng(currentLatitude, currentLongitude);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
            mMap.clear();
            MarkerOptions options = new MarkerOptions()
                    .position(latLng)
                    .title("You are here!");
            mMap.addMarker(options);
            findAllGP(new ParseGeoPoint(currentLatitude, currentLongitude));

        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE)
        {
            if(resultCode == RESULT_OK)
            {
                setResult(RESULT_OK);
                finish();
            }
        }
        else {
            autocompleteFragment.onActivityResult(requestCode, resultCode, data);
        }



    }
}
