package gpapp.martingdropin.com.gp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import gpapp.martingdropin.com.gp.adapters.PrescriptionAdapter;
import gpapp.martingdropin.com.gp.helper.Helpers;
import jp.wasabeef.recyclerview.animators.FadeInUpAnimator;

public class ViewPrescriptionsActivity extends AppCompatActivity {

    @Bind(R.id.recyclerViewPrescriptions)
    RecyclerView prescriptionsRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_prescriptions);
        ButterKnife.bind(this);
        fetchPrescriptions();

    }
    void fetchPrescriptions()
    {
        if(ParseUser.getCurrentUser()!=null) {
            final Helpers helpers = new Helpers(this);
            helpers.showLoadingDialog();
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Prescription");
            query.whereEqualTo("User", ParseUser.getCurrentUser());
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> appointmentList, ParseException e) {
                    helpers.dismissLoadingDialog();
                    if (e == null) {
                        setUpRecyclerViewWithData(appointmentList);
                    } else {
                        Helpers.showToast(getApplicationContext(), e.getMessage());
                    }
                }
            });
        }
    }
    void setUpRecyclerViewWithData(List<ParseObject> appointmentsList)
    {
        PrescriptionAdapter prescriptionAdapter = new PrescriptionAdapter(appointmentsList,this);
        prescriptionsRecyclerView.setItemAnimator(new FadeInUpAnimator());
        prescriptionsRecyclerView.setAdapter(prescriptionAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        prescriptionsRecyclerView.setLayoutManager(layoutManager);
        prescriptionsRecyclerView.setHasFixedSize(true);
    }
}
