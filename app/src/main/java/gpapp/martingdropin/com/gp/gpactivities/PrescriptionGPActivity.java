package gpapp.martingdropin.com.gp.gpactivities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import gpapp.martingdropin.com.gp.R;
import gpapp.martingdropin.com.gp.helper.Constants;
import gpapp.martingdropin.com.gp.helper.Helpers;

public class PrescriptionGPActivity extends AppCompatActivity {

    @Bind(R.id.patientEmail)
    EditText mPatientEmailTf;
    @Bind(R.id.patientName)
    TextView mPatientName;
    @Bind(R.id.patientAddress)
    TextView mPatientAddress;
    @Bind(R.id.prescriptionsText)
    EditText mPrescriptionTf;

    ParseUser mSelectedPatient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescription_gp);
        ButterKnife.bind(this);
    }

    @OnFocusChange(R.id.patientEmail)
    void emailEntered(View v ,boolean hasFocus)
    {
        if(!hasFocus)
        {
            final Helpers helpers = new Helpers(this);
            helpers.showLoadingDialog();
            ParseQuery<ParseUser> query = ParseUser.getQuery();
            query.whereEqualTo("email", mPatientEmailTf.getText().toString());
            query.whereEqualTo("isGP",false);
            query.getFirstInBackground(new GetCallback<ParseUser>() {
                @Override
                public void done(ParseUser object, ParseException e) {

                    if(e==null)
                    {
                        mSelectedPatient = object;
                        mPatientName.setText(object.getString(Constants.CONSTANT_APPOINTMENT_PATIENT_NAME_COLUMN));
                        ParseQuery<ParseObject> query = ParseQuery.getQuery("NormalUser");
                        query.whereEqualTo(Constants.USER_POINTER_COLUMN_CONSTANT, object);
                        query.getFirstInBackground(new GetCallback<ParseObject>() {
                            @Override
                            public void done(ParseObject object, ParseException e) {
                                helpers.dismissLoadingDialog();
                                if (e == null) {
                                    mPatientAddress.setText(object.getString(Constants.CONSTANT_NORMAL_ADDRESS_COLUMN));

                                } else
                                    Helpers.showToast(getApplicationContext(), e.getMessage());
                            }
                        });
                    }
                    else {
                        helpers.dismissLoadingDialog();
                        Helpers.showToast(getApplicationContext(), e.getMessage());
                    }
                }
            });
        }
    }
    @OnClick(R.id.confirmPrescription)
    void confirmPrescription()
    {
        if(mSelectedPatient!=null) {
            ParseObject parseObject = new ParseObject("Prescription");
            parseObject.put("prescriptions", mPrescriptionTf.getText().toString());
            parseObject.put("User", mSelectedPatient);
            parseObject.put("GP",ParseUser.getCurrentUser());
            parseObject.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if(e==null)
                    {
                        Helpers.showToast(getApplicationContext(),"Prescription sent to patient");
                        finish();
                    }
                    else
                        Helpers.showToast(getApplicationContext(),e.getMessage());
                }
            });
        }
        else
        {
            Helpers.showToast(this,"Please enter the email address of the patient");
        }
    }

}
