package gpapp.martingdropin.com.gp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import gpapp.martingdropin.com.gp.helper.Constants;
import gpapp.martingdropin.com.gp.helper.Helpers;

public class RegisterActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    @Bind(R.id.nameTf)
    EditText nameTf;
    @Bind(R.id.dateOfBirthTf)
    EditText mDOBTf;
    @Bind(R.id.genderTf)
    EditText mGenderTf;
    @Bind(R.id.addressText)
    EditText mAddressTf;
    @Bind(R.id.postCodeTf)
    EditText mPostCodeTf;
    @Bind(R.id.nhsNumberTf)
    EditText mNHSNumberTf;
    @Bind(R.id.emailTf)
    EditText mEmailTf;
    @Bind(R.id.passwordTf)
    EditText mPasswordTf;
    @Bind(R.id.currentGpAddress)
    EditText mCurrentGPAddressTf;
    @Bind(R.id.currentGpName)
    EditText mCurrentGPName;
    @Bind(R.id.contactTf)
    EditText mContactNumberTf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
    }

    @OnFocusChange(R.id.dateOfBirthTf)
    void selectedDateOfBirth(View v,boolean hasFocus)
    {
        if(hasFocus)
        {

                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        RegisterActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setThemeDark(true);
                dpd.vibrate(true);
                dpd.dismissOnPause(true);
                dpd.showYearPickerFirst(false);
                dpd.show(getFragmentManager(), "Datepickerdialog");
        }
    }
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth+"/"+(++monthOfYear)+"/"+year;
        mDOBTf.setText(date);
    }
    // Register button on click functionality
    @OnClick(R.id.registerUser)
    void registerUser()
    {
        //if(validateFields())
        {
            final Helpers helpers = new Helpers(this);
            helpers.showLoadingDialog();
            final ParseUser user = new ParseUser();
            user.setUsername(mEmailTf.getText().toString());
            user.setPassword(mPasswordTf.getText().toString());
            user.setEmail(mEmailTf.getText().toString());

            user.put(Constants.CONSTANT_APPOINTMENT_PATIENT_NAME_COLUMN,nameTf.getText().toString());
            user.put(Constants.ISGP_COLUMN_CONSTANT,false);
            user.signUpInBackground(new SignUpCallback() {
                public void done(ParseException e) {
                    if (e == null) {
                        // Hooray! Let them use the app now.
                        saveOtherDetails(user,helpers);
                       // helpers.showToast(getApplicationContext(),"Thank you for registering with us.Please confirm your email address");
                       // finish();
                    } else {
                        // Sign up didn't succeed. Look at the ParseException
                        // to figure out what went wrong
                        helpers.dismissLoadingDialog();
                        Helpers.showToast(getApplicationContext(),e.getMessage());
                    }
                }
            });
        }
    }
    void saveOtherDetails(ParseUser user, final Helpers helpers)
    {
        ParseObject gpParseObject = new ParseObject("NormalUser");

        gpParseObject.put(Constants.USER_POINTER_COLUMN_CONSTANT,user);
        gpParseObject.put(Constants.CONSTANT_NORMAL_NAME_COLUMN,nameTf.getText().toString());
        gpParseObject.put(Constants.CONSTANT_NORMAL_DOB_COLUMN,mDOBTf.getText().toString());
        gpParseObject.put(Constants.CONSTANT_NORMAL_ADDRESS_COLUMN,mAddressTf.getText().toString());
        gpParseObject.put(Constants.CONSTANT_NORMAL_POSTCODE_COLUMN,mPostCodeTf.getText().toString());
        gpParseObject.put(Constants.CONSTANT_NORMAL_NHS_COLUMN,mNHSNumberTf.getText().toString());
        gpParseObject.put(Constants.CONSTANT_NORMAL_CONTACT_COLUMN,mContactNumberTf.getText().toString());
        gpParseObject.put(Constants.CONSTANT_NORMAL_CURRENT_GP_ADDRESS_COLUMN,mCurrentGPAddressTf.getText().toString());
        gpParseObject.put(Constants.CONSTANT_NORMAL_CURRENT_GP_NAME_COLUMN,mCurrentGPName.getText().toString());
        gpParseObject.put(Constants.CONSTANT_NORMAL_GENDER_COLUMN, mGenderTf.getText().toString().toLowerCase().equals("m"));
        gpParseObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                helpers.dismissLoadingDialog();
                if(e==null)
                {
                    Helpers.showToast(getApplicationContext(),"Thank you for registering with us.Please confirm your email address");
                    finish();
                }
                else
                    Helpers.showToast(getApplicationContext(),e.getMessage());
            }
        });
    }
    boolean validateFields()
    {
        if(mEmailTf.getText().toString().isEmpty() ||
                mPasswordTf.getText().toString().isEmpty() ||
                nameTf.getText().toString().isEmpty()||
                mContactNumberTf.getText().toString().isEmpty() ||
                mDOBTf.getText().toString().isEmpty()||
                mGenderTf.getText().toString().isEmpty() ||
                mAddressTf.getText().toString().isEmpty()||
                mPostCodeTf.getText().toString().isEmpty()||
                mDOBTf.getText().toString().isEmpty()||
                mNHSNumberTf.getText().toString().isEmpty()
                )
        {
            Helpers.showToast(this,"Some Fields have been left blank");
            return false;
        }
        else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mEmailTf.getText().toString()).matches())
        {
            Helpers.showToast(this,"Invalid Email Address");
            return false;
        }
        else if(!mGenderTf.getText().toString().toLowerCase().equals("f") || !mGenderTf.getText().toString().toLowerCase().equals("m"))
        {
            mGenderTf.setText("");
            Helpers.showToast(this,"Please Enter M (Male) or F (Female)");
        }
        return true;
    }
}
