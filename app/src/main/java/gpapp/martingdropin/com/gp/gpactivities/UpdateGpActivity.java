package gpapp.martingdropin.com.gp.gpactivities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gpapp.martingdropin.com.gp.R;
import gpapp.martingdropin.com.gp.helper.Constants;
import gpapp.martingdropin.com.gp.helper.Helpers;

public class UpdateGpActivity extends AppCompatActivity {

    @Bind(R.id.emailTf)
    EditText mEmailTf;
    @Bind(R.id.addressTf)
    EditText mAddressTf;
    @Bind(R.id.phoneTf)
    EditText mPhoneTf;
    @Bind(R.id.fullNameTf)
    EditText mNameTf;

    ParseObject GPUserObject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_gp);
        ButterKnife.bind(this);
        fetchGPUserDetails();
    }
    // Register button on click functionality
    @OnClick(R.id.updateAccountGPButton)
    void updateGpAccount()
    {
        if(validateFields())
        {
            final ParseUser user = ParseUser.getCurrentUser();
            if(user!=null) {
                updateUserDetails(user);
            }
        }
    }

    void fetchGPUserDetails()
    {
        final ParseUser user = ParseUser.getCurrentUser();
        if(user!=null) {
            final Helpers helpers = new Helpers(this);
            helpers.showLoadingDialog();
            ParseQuery<ParseObject> query = ParseQuery.getQuery("GPUser");
            query.whereEqualTo(Constants.USER_POINTER_COLUMN_CONSTANT, user);
            query.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                    helpers.dismissLoadingDialog();
                    if(e==null) {
                        GPUserObject = object;
                        showUserDetails(user);
                    }
                    else
                        Helpers.showToast(getApplicationContext(),e.getMessage());
                }
            });
        }
    }

    void showUserDetails(ParseUser user)
    {
        mAddressTf.setText(GPUserObject.getString(Constants.ADDRESS_COLUMN_CONSTANT));
        mPhoneTf.setText(GPUserObject.getString(Constants.CONTACT_COLUMN_CONSTANT));
        mNameTf.setText(GPUserObject.getString(Constants.NAME_COLUMN_CONSTANT));
        mEmailTf.setText(user.getEmail());
    }

    void updateUserDetails(ParseUser user)
    {
        if(GPUserObject!=null)
        {
           final Helpers helpers = new Helpers(this);
            helpers.showLoadingDialog();
            GPUserObject.put(Constants.ADDRESS_COLUMN_CONSTANT, mAddressTf.getText().toString());
            GPUserObject.put(Constants.CONTACT_COLUMN_CONSTANT, mPhoneTf.getText().toString());
            GPUserObject.put(Constants.NAME_COLUMN_CONSTANT, mNameTf.getText().toString());
            GPUserObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                helpers.dismissLoadingDialog();
                                Helpers.showToast(getApplicationContext(), getString(R.string.update_success_message));
                            } else {
                                helpers.dismissLoadingDialog();
                                Helpers.showToast(getApplicationContext(), e.getMessage());
                            }
                        }
                    });
        }
    }
    boolean validateFields()
    {
        if(mPhoneTf.getText().toString().isEmpty() ||
                mNameTf.getText().toString().isEmpty()||
                mAddressTf.getText().toString().isEmpty())
        {
            Helpers.showToast(this, "All fields are compulsory");
            return false;
        }

        return true;
    }
}
