package gpapp.martingdropin.com.gp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import gpapp.martingdropin.com.gp.R;

/**
 * Created by RAJESHGUPTA on 22/02/16.
 */
public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuViewHolder>{

    private String[] menu;
    private static RecyclerViewListener sRecyclerViewListener;
    public MenuAdapter(String[] menuItems, RecyclerViewListener listener)
    {
        menu = menuItems;
        sRecyclerViewListener = listener;
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener
    {
        private TextView mMenuText;
        public MenuViewHolder(View itemView) {
            super(itemView);
            mMenuText = (TextView) itemView.findViewById(R.id.menuName);
            itemView.setOnClickListener(this);
        }
        public void bindMenu(int position)
        {
            String menuItem = menu[position];
            mMenuText.setText(menuItem);
        }
        @Override
        public void onClick(View v) {
            sRecyclerViewListener.recyclerListViewClicked(v, this.getAdapterPosition());
        }

    }
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_list_item,null,false);

        MenuViewHolder viewHolder = new MenuViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MenuViewHolder holder, int position) {
        holder.bindMenu(position);
    }

    @Override
    public int getItemCount() {
        return menu.length;
    }

    public interface RecyclerViewListener
    {
        public void recyclerListViewClicked(View v, int position);
    }
}
