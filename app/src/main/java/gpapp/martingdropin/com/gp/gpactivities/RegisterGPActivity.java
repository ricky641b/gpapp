package gpapp.martingdropin.com.gp.gpactivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import gpapp.martingdropin.com.gp.R;
import gpapp.martingdropin.com.gp.helper.Constants;
import gpapp.martingdropin.com.gp.helper.Helpers;

public class RegisterGPActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    @Bind(R.id.employeeIdTf)
    EditText mEmployeeIdTf;
    @Bind(R.id.emailTf)
    EditText mEmailTf;
    @Bind(R.id.passwordTf)
    EditText mPasswordTf;
    @Bind(R.id.addressTf)
    EditText mAddressTf;
    @Bind(R.id.phoneTf)
    EditText mPhoneTf;
    @Bind(R.id.fullNameTf)
    EditText mNameTf;

    int PLACE_PICKER_REQUEST = 1;
    private GoogleApiClient mGoogleApiClient;
    ParseGeoPoint mParseGeoPoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_gp);
        ButterKnife.bind(this);
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

    }
    // Register button on click functionality
    @OnFocusChange(R.id.addressTf)
    void startAddressActivity(View v, boolean hasFocus)
    {
        if(hasFocus) {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            try {
                startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        }
        //Intent addressActivty = new Intent(RegisterGPActivity.this, MapActivity.class);
        //startActivity(addressActivty);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                mAddressTf.setText(place.getAddress());
                mParseGeoPoint = new ParseGeoPoint(place.getLatLng().latitude,place.getLatLng().longitude);
            }
        }
    }
    @OnClick(R.id.registerButton)
    void registerGP()
    {
        if(validateFields())
        {
            final Helpers helpers = new Helpers(this);
            helpers.showLoadingDialog();
            final ParseUser user = new ParseUser();
            user.setUsername(mEmailTf.getText().toString());
            user.setPassword(mPasswordTf.getText().toString());
            user.setEmail(mEmailTf.getText().toString());

            user.put("gpName",mNameTf.getText().toString());
            user.put(Constants.EMPLOYEEID_COLUMN_CONSTANT,mEmployeeIdTf.getText().toString());
            user.put(Constants.ISGP_COLUMN_CONSTANT,true);
            user.signUpInBackground(new SignUpCallback() {
                public void done(ParseException e) {
                    if (e == null) {
                        // Hooray! Let them use the app now.
                       saveOtherDetails(user,helpers);
                    } else {
                        // Sign up didn't succeed. Look at the ParseException
                        // to figure out what went wrong
                        helpers.dismissLoadingDialog();
                        Helpers.showToast(getApplicationContext(),e.getMessage());
                    }
                }
            });
        }
    }
    void saveOtherDetails(ParseUser user, final Helpers helpers)
    {
        ParseObject gpParseObject = new ParseObject("GPUser");
        gpParseObject.put(Constants.USER_POINTER_COLUMN_CONSTANT, user);
        gpParseObject.put(Constants.ADDRESS_COLUMN_CONSTANT,mAddressTf.getText().toString());
        gpParseObject.put(Constants.CONTACT_COLUMN_CONSTANT,mPhoneTf.getText().toString());
        gpParseObject.put(Constants.NAME_COLUMN_CONSTANT,mNameTf.getText().toString());
        gpParseObject.put("email",mEmailTf.getText().toString());
        if(mParseGeoPoint!=null)
        {
            gpParseObject.put("location",mParseGeoPoint);
        }
        gpParseObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e==null)
                {
                    helpers.dismissLoadingDialog();
                    Helpers.showToast(getApplicationContext(),"Thank you for registering with us.Please confirm your email address");
                    finish();
                }
                else
                {
                    helpers.dismissLoadingDialog();
                    Helpers.showToast(getApplicationContext(),e.getMessage());
                }
            }
        });
    }
    boolean validateFields()
    {
        if(mEmployeeIdTf.getText().toString().isEmpty() ||
                mEmailTf.getText().toString().isEmpty() ||
                mPasswordTf.getText().toString().isEmpty()||
                mPhoneTf.getText().toString().isEmpty() ||
                mNameTf.getText().toString().isEmpty()||
                mAddressTf.getText().toString().isEmpty())
        {
            Helpers.showToast(this,"All fields are compulsory");
            return false;
        }
        else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mEmailTf.getText().toString()).matches())
        {
            Helpers.showToast(this,"Invalid Email Address");
            return false;
        }
        return true;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Helpers.showToast(this,"Sorry failed to connect to google services");
    }
}
