package gpapp.martingdropin.com.gp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gpapp.martingdropin.com.gp.helper.Constants;
import gpapp.martingdropin.com.gp.helper.Helpers;

public class GpDetailActivity extends AppCompatActivity {

    @Bind(R.id.gpName)
    TextView mGpNameText;
    @Bind(R.id.gpEmail)
    TextView mGpEmailText;
    @Bind(R.id.gpAddress)
    TextView mGpAddressText;
    @Bind(R.id.gpContact)
    TextView mGpContactText;

    String mReasonString;
    String mDateString;
    String jsonString;
    ParseObject mGpUserObject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gp_detail);
        ButterKnife.bind(this);
        jsonString = getIntent().getStringExtra("objectId");
        fetchPrescriptionDetail(jsonString);
        mReasonString = getIntent().getExtras().getString("REASON");
        mDateString = getIntent().getExtras().getString("DATE");
        Log.d("SSS", jsonString);
    }
    void fetchPrescriptionDetail(final String objectId)
    {
        final Helpers helpers =new Helpers(this);
        helpers.showLoadingDialog();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GPUser");
        query.whereEqualTo("objectId",objectId);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                helpers.dismissLoadingDialog();
                if (e == null) {
                    mGpUserObject = object;
                    mGpNameText.setText("GP Name: " + object.getString("fullName"));
                    mGpEmailText.setText("Email: " + object.getString("email"));
                    mGpAddressText.setText("Address: " + object.getString("address"));
                    mGpContactText.setText("Contact: " + object.getString("contact"));
                } else {

                    Helpers.showToast(getApplicationContext(), e.getMessage());
                }
            }
        });
    }
    @OnClick(R.id.bookAppointment)
    void bookAppointment() {
        if(mGpUserObject!=null) {
            ParseObject appointmentObject = new ParseObject("AppointMent");
            appointmentObject.put("reason", mReasonString);
            appointmentObject.put("dateOfAppointment", mDateString);
            appointmentObject.put("User", ParseUser.getCurrentUser());
            appointmentObject.put("GP", mGpUserObject);
            appointmentObject.put("patientName", ParseUser.getCurrentUser().getString(Constants.CONSTANT_APPOINTMENT_PATIENT_NAME_COLUMN));
            appointmentObject.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        Helpers.showToast(getApplicationContext(), "Your appointment has been booked successfully");
                        setResult(RESULT_OK);
                        finish();
                    } else {
                        Helpers.showToast(getApplicationContext(), e.getMessage());
                    }
                }
            });
        }
        else
        {
            Helpers.showToast(this,"Not valid GP Selected");
        }
    }
}
