package gpapp.martingdropin.com.gp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import gpapp.martingdropin.com.gp.adapters.NewsAdapter;
import gpapp.martingdropin.com.gp.helper.Helpers;
import jp.wasabeef.recyclerview.animators.FadeInUpAnimator;

public class NewsUpdateActivity extends AppCompatActivity {


    @Bind(R.id.recyclerViewNews)
    RecyclerView newsRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_update);
        ButterKnife.bind(this);
        fetchNews();

    }
    void fetchNews()
    {
        final Helpers helpers = new Helpers(this);
        helpers.showLoadingDialog();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("News");
        query.setLimit(50);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> newsList, ParseException e) {
                helpers.dismissLoadingDialog();
                if (e == null) {

                    setUpRecyclerViewWithData(newsList);
                } else {
                    Helpers.showToast(getApplicationContext(),e.getMessage());
                }
            }
        });
    }
    void setUpRecyclerViewWithData(List<ParseObject> newsList)
    {
        NewsAdapter newsAdapter = new NewsAdapter(newsList,this);
        newsRecyclerView.setItemAnimator(new FadeInUpAnimator());
        newsRecyclerView.setAdapter(newsAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        newsRecyclerView.setLayoutManager(layoutManager);
        newsRecyclerView.setHasFixedSize(true);
    }
}
