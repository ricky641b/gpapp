package gpapp.martingdropin.com.gp.gpactivities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import gpapp.martingdropin.com.gp.R;
import gpapp.martingdropin.com.gp.adapters.AppointmentsAdapter;
import gpapp.martingdropin.com.gp.helper.Constants;
import gpapp.martingdropin.com.gp.helper.Helpers;
import jp.wasabeef.recyclerview.animators.FadeInUpAnimator;

public class AppointmentsGPActivity extends AppCompatActivity {

    @Bind(R.id.recyclerViewAppointments)
    RecyclerView appointmentsRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointments_gp);
        ButterKnife.bind(this);
        fetchAppointments();

    }
    void fetchAppointments()
    {
        if(ParseUser.getCurrentUser()!=null) {
            final Helpers helpers = new Helpers(this);
            helpers.showLoadingDialog();
            ParseQuery<ParseObject> query = ParseQuery.getQuery("AppointMent");
            query.whereEqualTo(Constants.CONSTANT_APPOINTMENT_GP_COLUMN, ParseUser.getCurrentUser());
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> appointmentList, ParseException e) {
                    helpers.dismissLoadingDialog();
                    if (e == null) {
                        setUpRecyclerViewWithData(appointmentList);
                    } else {
                        Helpers.showToast(getApplicationContext(), e.getMessage());
                    }
                }
            });
        }
    }
    void setUpRecyclerViewWithData(List<ParseObject> appointmentsList)
    {
        AppointmentsAdapter newsAdapter = new AppointmentsAdapter(appointmentsList);
        appointmentsRecyclerView.setItemAnimator(new FadeInUpAnimator());
        appointmentsRecyclerView.setAdapter(newsAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        appointmentsRecyclerView.setLayoutManager(layoutManager);
        appointmentsRecyclerView.setHasFixedSize(true);
    }
}
