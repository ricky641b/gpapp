package gpapp.martingdropin.com.gp.helper;

/**
 * Created by RAJESHGUPTA on 22/02/16.
 */
public class Constants {

    public static final String ISGP_COLUMN_CONSTANT = "isGP";
    public static final String USER_POINTER_COLUMN_CONSTANT = "userPointer";
    public static final String ADDRESS_COLUMN_CONSTANT = "address";
    public static final String CONTACT_COLUMN_CONSTANT = "contact";
    public static final String NAME_COLUMN_CONSTANT = "fullName";
    public static final String EMPLOYEEID_COLUMN_CONSTANT = "employeeID";
    public static final String NEWS_TITLE_COLUMN_CONSTANT = "title";
    public static final String NEWS_DESCRIPTION_COLUMN_CONSTANT = "description";
    public static final String NEWS_LINK_COLUMN_CONSTANT = "link";


    public static final String CONSTANT_NORMAL_NAME_COLUMN = "fullName";
    public static final String CONSTANT_NORMAL_DOB_COLUMN = "DOB";
    public static final String CONSTANT_NORMAL_GENDER_COLUMN = "isMale";
    public static final String CONSTANT_NORMAL_ADDRESS_COLUMN = "address";
    public static final String CONSTANT_NORMAL_POSTCODE_COLUMN = "postcode";
    public static final String CONSTANT_NORMAL_NHS_COLUMN = "nhsnumber";
    public static final String CONSTANT_NORMAL_CONTACT_COLUMN = "contact";
    public static final String CONSTANT_NORMAL_CURRENT_GP_ADDRESS_COLUMN = "current_gp_address";
    public static final String CONSTANT_NORMAL_CURRENT_GP_NAME_COLUMN = "current_gp_name";


    public static final String CONSTANT_APPOINTMENT_REASON_COLUMN = "reason";
    public static final String CONSTANT_APPOINTMENT_DOB_COLUMN = "dateOfAppointment";
    public static final String CONSTANT_APPOINTMENT_USER_COLUMN = "User";
    public static final String CONSTANT_APPOINTMENT_GP_COLUMN = "GP";
    public static final String CONSTANT_APPOINTMENT_PATIENT_NAME_COLUMN = "patientName";


    public static final String[] GP_MENU_ARRAY= new String[]{
            "Emails" ,
            "Appointments",
            "Prescription",
            "View Feedback",
            "News Update",
            "Update Account",
            "Logout"
    };
    public static final String[] USER_MENU_ARRAY= new String[]{
            "Book Appointment" ,
            "Find GP",
            "View Prescriptions",
            "News Update",
            "Update Account",
            "Logout"
    };


}
