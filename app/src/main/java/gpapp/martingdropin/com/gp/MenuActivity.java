package gpapp.martingdropin.com.gp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.parse.ParseUser;

import gpapp.martingdropin.com.gp.adapters.MenuAdapter;
import gpapp.martingdropin.com.gp.gpactivities.MapActivity;
import gpapp.martingdropin.com.gp.helper.Constants;
import it.gmariotti.recyclerview.adapter.AlphaAnimatorAdapter;

public class MenuActivity extends AppCompatActivity implements MenuAdapter.RecyclerViewListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        MenuAdapter menuAdapter = new MenuAdapter(Constants.USER_MENU_ARRAY,this);
        AlphaAnimatorAdapter animatorAdapter = new AlphaAnimatorAdapter(menuAdapter,recyclerView);
        recyclerView.setAdapter(animatorAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
    }

    @Override
    public void recyclerListViewClicked(View v, int position) {
        switch (position)
        {
            case 0:
                Intent emailIntent =  new Intent(this,BookAppointmentActivity.class);
                startActivity(emailIntent);
                break;
            case 1:
                Intent findGPIntent =  new Intent(this,MapActivity.class);
                startActivity(findGPIntent);
                break;
            case 2:
                Intent prescriptionIntent =  new Intent(this,ViewPrescriptionsActivity.class);
                startActivity(prescriptionIntent);
                break;
            case 3:
                Intent newsUpdateIntent =  new Intent(this,NewsUpdateActivity.class);
                startActivity(newsUpdateIntent);
                break;
            case 4:
                Intent updateAccountIntent =  new Intent(this,AccountUpdateActivity.class);
                startActivity(updateAccountIntent);
                break;
            case 5:
                finish();
                ParseUser.logOut();

        }
    }

    @Override
    public void onBackPressed() {

    }
}
