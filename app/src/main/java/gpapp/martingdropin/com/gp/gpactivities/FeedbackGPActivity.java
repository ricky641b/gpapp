package gpapp.martingdropin.com.gp.gpactivities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import gpapp.martingdropin.com.gp.R;
import gpapp.martingdropin.com.gp.adapters.FeedbackAdapter;
import gpapp.martingdropin.com.gp.helper.Helpers;
import jp.wasabeef.recyclerview.animators.FadeInUpAnimator;

public class FeedbackGPActivity extends AppCompatActivity {

    @Bind(R.id.recyclerViewFeedback)
    RecyclerView feedbackRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_gp);
        ButterKnife.bind(this);
        fetchFeedback();
    }
    void fetchFeedback()
    {
        if(ParseUser.getCurrentUser()!=null) {
            final Helpers helpers = new Helpers(this);
            helpers.showLoadingDialog();
            ParseQuery<ParseObject> query = ParseQuery.getQuery("FeedBack");
            query.whereEqualTo("gpUserPointer", ParseUser.getCurrentUser());
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> appointmentList, ParseException e) {
                    helpers.dismissLoadingDialog();
                    if (e == null) {
                        setUpRecyclerViewWithData(appointmentList);
                    } else {
                        Helpers.showToast(getApplicationContext(), e.getMessage());
                    }
                }
            });
        }
    }
    void setUpRecyclerViewWithData(List<ParseObject> feedbackList)
    {
        FeedbackAdapter feedbackAdapter = new FeedbackAdapter(feedbackList);
        feedbackRecyclerView.setItemAnimator(new FadeInUpAnimator());
        feedbackRecyclerView.setAdapter(feedbackAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        feedbackRecyclerView.setLayoutManager(layoutManager);
        feedbackRecyclerView.setHasFixedSize(true);
    }
}
