package gpapp.martingdropin.com.gp;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gpapp.martingdropin.com.gp.helper.Constants;
import gpapp.martingdropin.com.gp.helper.Helpers;

public class PrescriptionDetailActivity extends AppCompatActivity {

    @Bind(R.id.gpName)
    TextView gpNameText;
    @Bind(R.id.name)
    TextView userNameText;
    @Bind(R.id.dob)
    TextView DOBText;
    @Bind(R.id.address)
    TextView addressText;
    @Bind(R.id.prescription)
    TextView prescriptionText;

    ParseUser gpUserObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescription_detail);
        ButterKnife.bind(this);
        String objectId = getIntent().getExtras().getString("objectId");
        fetchPrescriptionDetail(objectId);
        fetchUserDetail();

    }
    void fetchPrescriptionDetail(final String objectId)
    {
        final Helpers helpers =new Helpers(this);
        helpers.showLoadingDialog();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Prescription");
        query.whereEqualTo("objectId",objectId);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if(e==null)
                {
                    prescriptionText.setText("Prescription: " + object.getString("prescriptions"));
                    gpUserObject = object.getParseUser("GP");
                    gpUserObject.fetchIfNeededInBackground(new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject object, ParseException e) {
                            helpers.dismissLoadingDialog();
                            if (e == null) {
                               gpNameText.setText("GP Name: Dr. " + gpUserObject.getString("gpName"));

                            } else {
                                Helpers.showToast(getApplicationContext(),e.getMessage());
                            }
                        }
                    });

                }
                else {
                    helpers.dismissLoadingDialog();
                    Helpers.showToast(getApplicationContext(), e.getMessage());
                }
            }
        });
    }
    void fetchUserDetail()
    {
        final Helpers helpers= new Helpers(this);
        helpers.showLoadingDialog();
        ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("NormalUser");
        parseQuery.whereEqualTo(Constants.USER_POINTER_COLUMN_CONSTANT, ParseUser.getCurrentUser());
        parseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                helpers.dismissLoadingDialog();
                if(e==null)
                {

                    addressText.setText("Address: " + object.getString(Constants.CONSTANT_NORMAL_ADDRESS_COLUMN));
                    DOBText.setText("D.O.B: " + object.getString(Constants.CONSTANT_NORMAL_DOB_COLUMN));
                    userNameText.setText("Patient Name: " + object.getString(Constants.CONSTANT_NORMAL_NAME_COLUMN));
                }
                else
                    Helpers.showToast(getApplicationContext(),e.getMessage());
            }
        });
    }
    @OnClick(R.id.leaveFeedbackButton)
    void leaveFeedback()
    {
        if(gpUserObject!=null)
            showFeedbackInterface();
    }

    void showFeedbackInterface()
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(PrescriptionDetailActivity.this);
        alertDialog.setTitle("Feedback");
        alertDialog.setMessage("Enter feedback for the GP");

        final EditText input = new EditText(PrescriptionDetailActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        alertDialog.setPositiveButton("Submit",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String feedback = input.getText().toString();
                        if (!feedback.isEmpty()) {
                            submitFeedback(feedback);
                        } else
                            Helpers.showToast(getApplicationContext(),"No feedback given");
                    }
                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }
    void submitFeedback(String feedback)
    {
        final Helpers helpers= new Helpers(this);
        helpers.showLoadingDialog();
        ParseObject feedbackObject = new ParseObject("FeedBack");
        feedbackObject.put("feedback",feedback);
        feedbackObject.put("userPointer",ParseUser.getCurrentUser());
        feedbackObject.put("gpUserPointer",gpUserObject);
        feedbackObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                helpers.dismissLoadingDialog();
                if(e==null)
                {
                    Helpers.showToast(getApplicationContext(),"Feedback left for the GP");
                }
                else
                {
                    Helpers.showToast(getApplicationContext(),e.getMessage());
                }
            }
        });
    }
}
