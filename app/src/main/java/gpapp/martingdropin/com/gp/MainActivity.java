package gpapp.martingdropin.com.gp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gpapp.martingdropin.com.gp.gpactivities.GPMenuActivity;
import gpapp.martingdropin.com.gp.gpactivities.RegisterGPActivity;
import gpapp.martingdropin.com.gp.helper.Constants;
import gpapp.martingdropin.com.gp.helper.Helpers;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.emailTf)
    EditText emailTextField;
    @Bind(R.id.passwordTf)
    EditText passwordTextField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        if(ParseUser.getCurrentUser()!=null)
        {
            if(ParseUser.getCurrentUser().getBoolean(Constants.ISGP_COLUMN_CONSTANT))
            {
                Intent homeClass = new Intent(MainActivity.this, GPMenuActivity.class);
                startActivity(homeClass);
            }
            else
            {
                Intent homeClass = new Intent(MainActivity.this, MenuActivity.class);
                startActivity(homeClass);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @OnClick(R.id.signInButton)
    void logIn()
    {
        if(validateFields()) {

            final Helpers helpers = new Helpers(this);
            helpers.showLoadingDialog();
            ParseUser.logInInBackground(emailTextField.getText().toString(), passwordTextField.getText().toString(), new LogInCallback() {
                public void done(ParseUser user, ParseException e) {
                    helpers.dismissLoadingDialog();
                    if (user != null) {
                        // Hooray! The user is logged in.
                        if(user.getBoolean(Constants.ISGP_COLUMN_CONSTANT))
                        {
                            Intent homeClass = new Intent(MainActivity.this, GPMenuActivity.class);
                            startActivity(homeClass);
                        }
                        else
                        {
                            Intent homeClass = new Intent(MainActivity.this, MenuActivity.class);
                            startActivity(homeClass);
                        }

                    } else {
                        // Signup failed. Look at the ParseException to see what happened.
                        Helpers.showToast(getApplicationContext(), e.getMessage());
                    }
                }
            });
        }
    }
    boolean validateFields()
    {
        if(emailTextField.getText().toString().isEmpty() || passwordTextField.getText().toString().isEmpty())
        {
            Helpers.showToast(this,"Email or Password is empty");
            return false;
        }

        return true;
    }
    @OnClick(R.id.registerGP)
    void registerGpClicked()
    {
        Intent registerGP = new Intent(this, RegisterGPActivity.class);
        startActivity(registerGP);
    }
    @OnClick(R.id.registerButton)
    void registerClicked()
    {
        Intent registerGP = new Intent(this, RegisterActivity.class);
        startActivity(registerGP);
    }
}
