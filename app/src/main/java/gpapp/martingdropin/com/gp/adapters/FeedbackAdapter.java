package gpapp.martingdropin.com.gp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.List;

import gpapp.martingdropin.com.gp.R;
import gpapp.martingdropin.com.gp.helper.Constants;

/**
 * Created by RAJESHGUPTA on 22/02/16.
 */
public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.FeedBackViewHolder>{

    private List<ParseObject> feedbackLists;
    public FeedbackAdapter(List<ParseObject> newsList)
    {
        feedbackLists = newsList;
    }

    public class FeedBackViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener
    {
        private TextView mFeedback;

        public FeedBackViewHolder(View itemView) {
            super(itemView);
            mFeedback = (TextView) itemView.findViewById(R.id.feedbackText);
            itemView.setOnClickListener(this);
        }
        public void bindNews(int position)
        {
            final ParseObject appointmentObject = feedbackLists.get(position);
            final ParseUser parseUser= appointmentObject.getParseUser(Constants.USER_POINTER_COLUMN_CONSTANT);
            try
            {
                parseUser.fetchIfNeeded();
                mFeedback.setText(appointmentObject.getString("feedback") + " by " + parseUser.getString("patientName"));
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
           /* parseUser.fetchIfNeededInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                    if (e == null)

                }
            });*/

        }
        @Override
        public void onClick(View v) {

        }

    }
    public FeedBackViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.feedback_list_item,parent,false);
        FeedBackViewHolder viewHolder = new FeedBackViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(FeedBackViewHolder holder, int position)
    {
        holder.bindNews(position);
    }

    @Override
    public int getItemCount() {
        return feedbackLists.size();
    }

}

